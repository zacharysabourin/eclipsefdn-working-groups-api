-- efclone.OrganizationTransactions definition
CREATE DATABASE efclone;
USE efclone;

CREATE TABLE `OrganizationTransactions` (
  `OrganizationID` int(10) unsigned NOT NULL,
  `TransactionID` int(10) unsigned NOT NULL,
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `DocumentID` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`,`TransactionID`,`Relation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.Organizations definition

CREATE TABLE `Organizations` (
  `OrganizationID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name1` varchar(255) NOT NULL DEFAULT '',
  `Name2` varchar(255) NOT NULL DEFAULT '',
  `Phone` varchar(40) NOT NULL DEFAULT '',
  `Fax` varchar(40) NOT NULL DEFAULT '',
  `Comments` mediumtext NOT NULL,
  `MemberSince` date DEFAULT NULL,
  `LAT_LONG` varchar(30) DEFAULT NULL,
  `SCRM_GUID` char(36) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`OrganizationID`),
  KEY `idx_SCRM_GUID` (`SCRM_GUID`)
) ENGINE=InnoDB AUTO_INCREMENT=1464 DEFAULT CHARSET=utf8mb3;


-- efclone.People definition

CREATE TABLE `People` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `FName` varchar(100) NOT NULL DEFAULT '',
  `LName` varchar(100) NOT NULL DEFAULT '',
  `Type` char(2) NOT NULL DEFAULT '',
  `IsMember` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `EMail` varchar(200) NOT NULL DEFAULT '',
  `Phone` varchar(40) DEFAULT NULL,
  `Fax` varchar(40) DEFAULT NULL,
  `Mobile` varchar(40) DEFAULT NULL,
  `Comments` mediumtext DEFAULT NULL,
  `Provisioning` mediumtext DEFAULT NULL,
  `IsUnixAcctCreated` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `MemberSince` date DEFAULT NULL,
  `IssuesPending` tinyint(1) DEFAULT 0,
  `MemberPasswd` varchar(40) DEFAULT NULL,
  `Latitude` varchar(40) DEFAULT NULL,
  `Longitude` varchar(40) DEFAULT NULL,
  `Blog` varchar(100) DEFAULT NULL,
  `Picture_mime` varchar(32) DEFAULT NULL,
  `Picture` blob DEFAULT NULL,
  `SCRM_GUID` char(36) DEFAULT NULL,
  PRIMARY KEY (`PersonID`),
  KEY `idx_people_email` (`EMail`),
  KEY `idx_people_FName` (`FName`),
  KEY `idx_people_LName` (`LName`),
  KEY `idx_SCRM_GUID` (`SCRM_GUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleEmails definition

CREATE TABLE `PeopleEmails` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `EmailID` int(10) unsigned NOT NULL DEFAULT 0,
  `Email` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`PersonID`,`EmailID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleGroups definition

CREATE TABLE `PeopleGroups` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `GroupID` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`PersonID`(50),`GroupID`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleHistory definition

CREATE TABLE `PeopleHistory` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PersonID` varchar(255) NOT NULL,
  `FName` varchar(100) DEFAULT NULL,
  `LName` varchar(100) DEFAULT NULL,
  `EMail` varchar(200) DEFAULT NULL,
  `newPersonID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25490 DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleTransactions definition

CREATE TABLE `PeopleTransactions` (
  `PersonID` varchar(255) NOT NULL,
  `TransactionID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`PersonID`,`TransactionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.People_to_Bugzilla definition

CREATE TABLE `People_to_Bugzilla` (
  `PersonID` varchar(255) DEFAULT NULL,
  `EMail` varchar(200) NOT NULL DEFAULT '',
  `userid` mediumint(9) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectGroups definition

CREATE TABLE `ProjectGroups` (
  `ProjectID` varchar(100) NOT NULL DEFAULT '',
  `GroupID` varchar(100) NOT NULL DEFAULT '',
  `IsDownload` tinyint(3) unsigned DEFAULT 0,
  PRIMARY KEY (`GroupID`,`ProjectID`),
  KEY `FK_PG_ProjectID` (`ProjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectGroups_old definition

CREATE TABLE `ProjectGroups_old` (
  `ProjectID` varchar(100) NOT NULL DEFAULT '',
  `GroupID` varchar(100) NOT NULL DEFAULT '',
  `IsDownload` tinyint(3) unsigned DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectReviews definition

CREATE TABLE `ProjectReviews` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectID` varchar(128) DEFAULT NULL,
  `ProjectName` varchar(128) DEFAULT NULL,
  `BugNumber` int(11) DEFAULT NULL,
  `ProjectTopLevel` varchar(128) DEFAULT NULL,
  `ProjectURL` varchar(256) DEFAULT NULL,
  `ProposalName` varchar(256) DEFAULT NULL,
  `ProposalURL` varchar(256) DEFAULT NULL,
  `IPLogURL` varchar(256) DEFAULT NULL,
  `ReviewAttendees` varchar(256) DEFAULT NULL,
  `ReviewDate` datetime DEFAULT NULL,
  `ReviewName` varchar(128) DEFAULT NULL,
  `SlidesURL` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2998 DEFAULT CHARSET=utf8mb3;


-- efclone.Projects definition

CREATE TABLE `Projects` (
  `ProjectID` varchar(100) NOT NULL DEFAULT '',
  `Name` varchar(200) NOT NULL DEFAULT '',
  `Level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `ParentProjectID` varchar(100) NOT NULL DEFAULT '',
  `Description` mediumtext NOT NULL,
  `UrlDownload` varchar(255) DEFAULT '',
  `UrlIndex` varchar(255) DEFAULT '',
  `DateActive` date NOT NULL DEFAULT '0000-00-00',
  `SortOrder` int(10) unsigned DEFAULT 0,
  `IsActive` tinyint(1) DEFAULT 1,
  `BugsName` varchar(64) DEFAULT NULL,
  `ProjectPhase` varchar(20) DEFAULT NULL,
  `DiskQuotaGB` int(10) unsigned DEFAULT 0,
  `IsComponent` tinyint(1) DEFAULT 0,
  `IsStandard` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ProjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_Countries definition

CREATE TABLE `SYS_Countries` (
  `CCode` char(2) NOT NULL DEFAULT '',
  `en_Description` varchar(200) NOT NULL DEFAULT '',
  `fr_Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`CCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_Documents definition

CREATE TABLE `SYS_Documents` (
  `DocumentID` varchar(20) NOT NULL DEFAULT '',
  `Version` double NOT NULL DEFAULT 0,
  `DocumentBLOB` mediumblob NOT NULL,
  `IsActive` tinyint(1) DEFAULT 1,
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Comments` mediumtext DEFAULT NULL,
  `Type` char(2) NOT NULL DEFAULT '',
  `ContentType` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`DocumentID`,`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_Dues definition

CREATE TABLE `SYS_Dues` (
  `DuesID` tinyint(3) unsigned DEFAULT NULL,
  `DuesAmount` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- efclone.SYS_Errors definition

CREATE TABLE `SYS_Errors` (
  `ErrorID` int(10) unsigned NOT NULL DEFAULT 0,
  `Language` char(2) NOT NULL DEFAULT '',
  `Message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ErrorID`,`Language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_Licenses definition

CREATE TABLE `SYS_Licenses` (
  `LicenseID` varchar(20) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `IsActive` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`LicenseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_ModLog definition

CREATE TABLE `SYS_ModLog` (
  `LogID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LogTable` varchar(100) NOT NULL DEFAULT '',
  `PK1` varchar(100) NOT NULL DEFAULT '',
  `PK2` varchar(100) NOT NULL DEFAULT '',
  `LogAction` varchar(255) NOT NULL DEFAULT '',
  `PersonID` varchar(255) DEFAULT NULL,
  `ModDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`LogID`),
  KEY `IDX_LogAction` (`LogAction`),
  KEY `IDX_LogTable` (`LogTable`),
  KEY `IDX_PK1` (`PK1`),
  KEY `IDX_PK2` (`PK2`)
) ENGINE=MyISAM AUTO_INCREMENT=4192098 DEFAULT CHARSET=utf8mb3;


-- efclone.SYS_Relations definition

CREATE TABLE `SYS_Relations` (
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `Description` varchar(100) NOT NULL DEFAULT '',
  `IsActive` tinyint(1) NOT NULL DEFAULT 0,
  `Type` char(2) NOT NULL DEFAULT '',
  `SortOrder` varchar(20) DEFAULT '',
  PRIMARY KEY (`Relation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.countries definition

CREATE TABLE `countries` (
  `geoname_id` varchar(255) NOT NULL,
  `locale_code` varchar(255) NOT NULL,
  `continent_code` varchar(255) NOT NULL,
  `continent_name` varchar(255) NOT NULL,
  `country_iso_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `is_in_european_union` tinyint(1) NOT NULL,
  PRIMARY KEY (`geoname_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.OrganizationAddresses definition

CREATE TABLE `OrganizationAddresses` (
  `OrganizationID` int(10) unsigned NOT NULL DEFAULT 0,
  `AddressID` int(10) unsigned NOT NULL DEFAULT 0,
  `Address1` varchar(200) NOT NULL DEFAULT '',
  `Address2` varchar(200) NOT NULL DEFAULT '',
  `Address3` varchar(200) NOT NULL DEFAULT '',
  `City` varchar(100) NOT NULL DEFAULT '',
  `ProvStateRegion` varchar(100) NOT NULL DEFAULT '',
  `PostalCode` varchar(100) NOT NULL DEFAULT '',
  `CCode` char(2) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`OrganizationID`,`AddressID`),
  KEY `FK_ccode` (`CCode`),
  CONSTRAINT `OrganizationAddresses_ibfk_1` FOREIGN KEY (`CCode`) REFERENCES `SYS_Countries` (`CCode`) ON UPDATE CASCADE,
  CONSTRAINT `fk_OrganizationID` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='InnoDB free: 4096 kB';


-- efclone.OrganizationContacts definition

CREATE TABLE `OrganizationContacts` (
  `OrganizationID` int(10) unsigned NOT NULL DEFAULT 0,
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `Comments` mediumtext DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`,`PersonID`,`Relation`),
  KEY `PersonID` (`PersonID`),
  KEY `Relation` (`Relation`),
  CONSTRAINT `FK_1` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON UPDATE CASCADE,
  CONSTRAINT `FK_3` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON UPDATE CASCADE,
  CONSTRAINT `OrganizationContacts_ibfk_1` FOREIGN KEY (`Relation`) REFERENCES `SYS_Relations` (`Relation`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.OrganizationDocuments definition

CREATE TABLE `OrganizationDocuments` (
  `OrganizationID` int(10) unsigned NOT NULL DEFAULT 0,
  `DocumentID` varchar(20) NOT NULL DEFAULT '',
  `Version` double NOT NULL DEFAULT 0,
  `EffectiveDate` date DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `ScannedDocumentBLOB` mediumblob DEFAULT NULL,
  `ScannedDocumentMime` varchar(75) DEFAULT NULL,
  `ScannedDocumentBytes` mediumint(8) unsigned DEFAULT NULL,
  `ScannedDocumentFileName` varchar(75) DEFAULT NULL,
  `Dues` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `Relation` varchar(5) DEFAULT NULL,
  `InvoiceMonth` tinyint(3) unsigned DEFAULT NULL,
  `Comments` mediumtext DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`,`DocumentID`),
  KEY `DocumentID` (`DocumentID`),
  CONSTRAINT `FK_OrganizationDocuments2` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrganizationDocuments_ibfk_1` FOREIGN KEY (`DocumentID`) REFERENCES `SYS_Documents` (`DocumentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.OrganizationEmploymentHistory definition

CREATE TABLE `OrganizationEmploymentHistory` (
  `OrganizationID` int(10) unsigned NOT NULL,
  `PersonID` varchar(255) NOT NULL,
  `EffectiveDate` date NOT NULL,
  `ExpirationDate` date DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`,`PersonID`,`EffectiveDate`),
  KEY `People_FK1` (`PersonID`),
  CONSTRAINT `People_FK1` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `People_FK2` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.OrganizationMemberships definition

CREATE TABLE `OrganizationMemberships` (
  `OrganizationID` int(10) unsigned NOT NULL DEFAULT 0,
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `EntryDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ExpiryDate` datetime DEFAULT NULL,
  `Comments` mediumtext DEFAULT NULL,
  `IssuesPending` tinyint(1) DEFAULT 0,
  `DuesTier` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `RenewalProb` varchar(2) DEFAULT 'y',
  `InvoiceMonth` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`OrganizationID`,`Relation`),
  KEY `Relation` (`Relation`),
  CONSTRAINT `OrganizationMemberships_ibfk_1` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrganizationMemberships_ibfk_2` FOREIGN KEY (`Relation`) REFERENCES `SYS_Relations` (`Relation`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.OrganizationRelations definition

CREATE TABLE `OrganizationRelations` (
  `OrganizationID` int(10) unsigned NOT NULL DEFAULT 0,
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `EntryDate` date DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`,`Relation`),
  KEY `Relation` (`Relation`),
  CONSTRAINT `FK_OrganizationRelations1` FOREIGN KEY (`OrganizationID`) REFERENCES `Organizations` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrganizationRelations_ibfk_1` FOREIGN KEY (`Relation`) REFERENCES `SYS_Relations` (`Relation`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleAddresses definition

CREATE TABLE `PeopleAddresses` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `AddressID` int(10) unsigned NOT NULL DEFAULT 0,
  `Address1` varchar(200) NOT NULL DEFAULT '',
  `Address2` varchar(200) NOT NULL DEFAULT '',
  `Address3` varchar(200) NOT NULL DEFAULT '',
  `City` varchar(100) NOT NULL DEFAULT '',
  `ProvStateRegion` varchar(100) NOT NULL DEFAULT '',
  `PostalCode` varchar(100) NOT NULL DEFAULT '',
  `CCode` char(2) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`PersonID`,`AddressID`),
  KEY `FK_ccode` (`CCode`),
  CONSTRAINT `PeopleAddresses_ibfk_1` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PeopleAddresses_ibfk_2` FOREIGN KEY (`CCode`) REFERENCES `SYS_Countries` (`CCode`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='InnoDB free: 4096 kB; (`id`) REFER `eclipsefoundation/people';


-- efclone.PeopleDocuments definition

CREATE TABLE `PeopleDocuments` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `DocumentID` varchar(20) NOT NULL DEFAULT '',
  `Version` double NOT NULL DEFAULT 0,
  `EffectiveDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ReceivedDate` date DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `ScannedDocumentBLOB` mediumblob DEFAULT NULL,
  `ScannedDocumentMime` varchar(75) DEFAULT NULL,
  `ScannedDocumentBytes` mediumint(8) unsigned DEFAULT NULL,
  `ScannedDocumentFileName` varchar(75) DEFAULT NULL,
  `Comments` mediumtext DEFAULT NULL,
  PRIMARY KEY (`PersonID`,`DocumentID`,`EffectiveDate`),
  KEY `DocumentID` (`DocumentID`),
  CONSTRAINT `PeopleDocuments_ibfk_1` FOREIGN KEY (`DocumentID`) REFERENCES `SYS_Documents` (`DocumentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PeopleDocuments_ibfk_2` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleProjects definition

CREATE TABLE `PeopleProjects` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `ProjectID` varchar(100) NOT NULL DEFAULT '',
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `ActiveDate` date NOT NULL DEFAULT '0000-00-00',
  `InactiveDate` date DEFAULT NULL,
  `EditBugs` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`PersonID`,`ProjectID`,`Relation`,`ActiveDate`),
  KEY `FK_1` (`ProjectID`),
  KEY `FK_3` (`Relation`),
  CONSTRAINT `PeopleProjects_ibfk_1` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PeopleProjects_ibfk_2` FOREIGN KEY (`ProjectID`) REFERENCES `Projects` (`ProjectID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PeopleProjects_ibfk_3` FOREIGN KEY (`Relation`) REFERENCES `SYS_Relations` (`Relation`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.PeopleRelations definition

CREATE TABLE `PeopleRelations` (
  `PersonID` varchar(255) NOT NULL DEFAULT '',
  `Relation` varchar(5) NOT NULL DEFAULT '',
  `EntryDate` date DEFAULT NULL,
  PRIMARY KEY (`PersonID`,`Relation`),
  KEY `fk_2` (`Relation`),
  CONSTRAINT `PeopleRelations_ibfk_1` FOREIGN KEY (`PersonID`) REFERENCES `People` (`PersonID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PeopleRelations_ibfk_2` FOREIGN KEY (`Relation`) REFERENCES `SYS_Relations` (`Relation`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectDocuments definition

CREATE TABLE `ProjectDocuments` (
  `ProjectID` varchar(100) NOT NULL,
  `DocumentID` varchar(20) NOT NULL DEFAULT '',
  `Version` double NOT NULL DEFAULT 0,
  `EffectiveDate` date DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `ScannedDocumentBLOB` mediumblob DEFAULT NULL,
  `ScannedDocumentMime` varchar(75) DEFAULT NULL,
  `ScannedDocumentBytes` mediumint(8) unsigned DEFAULT NULL,
  `ScannedDocumentFileName` varchar(75) DEFAULT NULL,
  `Comments` mediumtext DEFAULT NULL,
  PRIMARY KEY (`ProjectID`,`DocumentID`),
  KEY `DocumentID` (`DocumentID`),
  CONSTRAINT `FK_ProjectDocuments1` FOREIGN KEY (`DocumentID`) REFERENCES `SYS_Documents` (`DocumentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ProjectDocuments2` FOREIGN KEY (`ProjectID`) REFERENCES `Projects` (`ProjectID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectLicenses definition

CREATE TABLE `ProjectLicenses` (
  `ProjectID` varchar(100) NOT NULL,
  `LicenseID` varchar(20) NOT NULL DEFAULT 'EPL1.0',
  PRIMARY KEY (`ProjectID`,`LicenseID`),
  KEY `LicenseID` (`LicenseID`),
  CONSTRAINT `ProjectLicenses_ibfk_1` FOREIGN KEY (`ProjectID`) REFERENCES `Projects` (`ProjectID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ProjectLicenses_ibfk_2` FOREIGN KEY (`LicenseID`) REFERENCES `SYS_Licenses` (`LicenseID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectReviewStatus definition

CREATE TABLE `ProjectReviewStatus` (
  `ID` int(11) DEFAULT NULL,
  `Status` varchar(128) DEFAULT NULL,
  `Value` varchar(128) DEFAULT NULL,
  KEY `FK_ID` (`ID`),
  CONSTRAINT `FK_ID` FOREIGN KEY (`ID`) REFERENCES `ProjectReviews` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- efclone.ProjectServices definition

CREATE TABLE `ProjectServices` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProjectID` varchar(100) NOT NULL,
  `ServiceType` char(12) NOT NULL,
  `ServiceVersion` varchar(20) DEFAULT NULL,
  `ServicePreviousVersion` varchar(20) DEFAULT NULL,
  `ServerHost` varchar(200) NOT NULL,
  `ServerPort` mediumint(8) unsigned NOT NULL,
  `XvncBase` mediumint(8) unsigned DEFAULT NULL,
  `OtherData` mediumtext DEFAULT NULL,
  `State` char(12) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ProjectServices_ibfk_1` (`ProjectID`),
  CONSTRAINT `ProjectServices_ibfk_1` FOREIGN KEY (`ProjectID`) REFERENCES `Projects` (`ProjectID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8mb3;


-- efclone.country_blocks_ipv4 definition

CREATE TABLE `country_blocks_ipv4` (
  `network` varchar(255) NOT NULL,
  `geoname_id` varchar(255) NOT NULL,
  `registered_country_geoname_id` varchar(255) NOT NULL,
  `represented_country_geoname_id` varchar(255) NOT NULL,
  `is_anonymous_proxy` tinyint(1) NOT NULL,
  `is_satellite_provider` tinyint(1) NOT NULL,
  PRIMARY KEY (`network`),
  KEY `fk_geoname_id` (`geoname_id`),
  CONSTRAINT `fk_geoname_id` FOREIGN KEY (`geoname_id`) REFERENCES `countries` (`geoname_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;