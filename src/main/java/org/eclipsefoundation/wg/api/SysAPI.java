package org.eclipsefoundation.wg.api;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import io.quarkus.oidc.client.filter.OidcClientFilter;

/**
 * FoundationDB API system API binding.
 * 
 * @author Martin Lowe
 *
 */
@Path("sys")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface SysAPI {

    @GET
    @Path("relations")
    @RolesAllowed("fdb_read_sys")
    Response get(@BeanParam BaseAPIParameters baseParams, @QueryParam("type") String type);
}
