package org.eclipsefoundation.wg.api;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_SysRelationData.Builder.class)
public abstract class SysRelationData {
    public abstract String getRelation();

    public abstract String getDescription();

    public abstract boolean isActive();

    public abstract String getType();

    @Nullable
    public abstract String getSortOrder();

    public static Builder builder() {
        return new AutoValue_SysRelationData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setRelation(String relation);

        public abstract Builder setDescription(String description);

        public abstract Builder setActive(boolean isActive);

        public abstract Builder setType(String type);

        public abstract Builder setSortOrder(@Nullable String sortOrder);

        public abstract SysRelationData build();
    }
}
